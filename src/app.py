from flask import Flask, jsonify, request, json, render_template
from db_utils import get_db

app = Flask(__name__)

@app.route("/pokemon",  methods=['POST'])
def insertPokemons():
    db = get_db()
    cursor = db.cursor()
    with open('pokemon.csv','r') as file:
        print(file)
        no_records = 0
        for row in file:
            cursor.execute("INSERT INTO pokemons VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", row.split(","))
            db.commit()
            no_records += 1
        db.close()
        return no_records


@app.route("/pokemon",  methods=['GET'])
def getPokemon():
    db = get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM pokemons;")
    rows = cursor.fetchall()
    return jsonify(rows)


@app.route("/pokemonFilter",  methods=['GET'])
def getFilterPokemon():
        number = request.args.get('number', '', type = int)
        name = request.args.get('name', '', type = str)
        type1 = request.args.get('type1', '', type = str)
        type2 = request.args.get('type2', '', type = str)
        total = request.args.get('total', '', type = int)
        hp = request.args.get('hp', '', type = int)
        attack = request.args.get('attack', '', type = int)
        defense = request.args.get('defense', '', type = int)
        spAttack = request.args.get('spAttack', '', type = int)
        spDefense = request.args.get('spDefense', '', type = int)
        speed = request.args.get('speed', '', type = int)
        generation = request.args.get('generation', '', type = int)
        legendary = request.args.get('legendary', '', type = bool)

        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM pokemons WHERE number='{0}' and name='{1}'"
                        "and type1='{2}' and type2='{3}' and  total='{4}'"
                        "and hp='{5}' and attack='{6}' and  defense='{7}'"
                        "and sp_atk='{8}' and sp_def='{9}' and  speed='{10}'"
                        "and generation='{11}' and legendary='{12}'"
                        .format(number, name, type1, type2, 
                                total, hp, attack, defense, 
                                spAttack, spDefense,speed,
                                generation, legendary))
        rows = cursor.fetchall()
        return jsonify(rows)
    
    
if __name__ == "__main__":
    app.run(debug=True)
