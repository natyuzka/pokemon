import sqlite3, csv

DATABASE_NAME = "pruebaPokemons.sqlite3"

def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn
